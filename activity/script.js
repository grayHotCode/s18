
function Trainer(name, age, gender){
	this.name = name;
	this.age = age;
	this.gender = gender;
};

let sidney  = new Trainer("Sidney", 16, "Male");
console.log(sidney);

function Pokemon(name){
	this.name = name;
	this.health = 200;
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`);
		target.health -= 10;
		console.log(`${this.name}'s health is now ${target.health}`);
	}
};

let pickachu = new Pokemon("Pickachu");
let mewtwo = new Pokemon("Mewtwo");

pickachu.attack(mewtwo);

